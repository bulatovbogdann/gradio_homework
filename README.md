# Реализация графического интерфейся для приложения, предсказывающего эмоциональную окраску текста

## Запуск
**Развернуть сервис**
```
https://gitlab.com/bulatovbogdann/model_service_api 
```
**клонировать репозиторий**
```bash
git clone https://gitlab.com/bulatovbogdann/gradio_homework.git
cd gradio_homework
`````
**запустить приложение**
```bash
python -m app.py
````` 
