import gradio as gr
import requests

def predict_sentiment(text):
    # Адрес вашего API
    url = "http://127.0.0.1:7007/api/predict/"
    # Тело запроса
    payload = {"text": text}
    # Заголовки запроса
    headers = {"Content-Type": "application/json"}

    # Отправка POST запроса
    response = requests.post(url, json=payload, headers=headers)
    # Проверка статуса ответа
    if response.status_code == 200:
        # Если запрос успешен, возвращаем предсказанное настроение
        result = response.json()
        return result
    else:
        # Если запрос не успешен, возвращаем сообщение об ошибке
        return f"Ошибка: {response.status_code}"

# Создание интерфейса Gradio
iface = gr.Interface(fn=predict_sentiment, inputs="text", outputs="text")

# Запуск интерфейса
iface.launch()
